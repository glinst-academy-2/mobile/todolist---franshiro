$(document).ready(function () {
  loadTodo()
  readyFn()
  $('[data-toggle="tooltip"]').tooltip()
});

function readyFn(){
  hideButton()
}

function hideButton(){
  let token = localStorage.getItem('token')
  if(!token){
    $('#newTodo').append(`
      <button class="add-new-todo" data-toggle="modal" data-target="#addTodo">
        <i class="fas fa-plus"></i>
          ADD TO-DO
      </button>
    `)
  }
  else{
    $('#newTodo').empty()
  }
}

function loadTodo(){
  let database = JSON.parse(localStorage.getItem('database'))

    $('#isiWeb').empty()
    if(database){
      database.forEach(list => {
        let finish = ""
        if(list.isFinish === true){
          finish = "alert alert-success"
        }
        else{
          finish = "alert alert-dark"
        }
        $('#isiWeb').append(`
          <div class="${finish}" role="alert" align="center" onclick="getInfo('${list.title}')" data-toggle="modal" data-target="#detailTodo">
            ${list.title}
          </div> 
        `)
      })
    }
}

function addTodo(){
  let database = JSON.parse(localStorage.getItem('database'))
  let data = []
  let object = {
    title : $('#todoTitle').val(),
    description : $('#todoDescription').val(),
    dueDate : $('#datepicker').val(),
    isFinish : false
  }
  
  if(database){
    for(let i = 0; i < database.length; i++){
      data.push(database[i])
    }
    data.unshift(object)
    localStorage.setItem('database', JSON.stringify(data))
    loadTodo()
  } else {
    data.unshift(object)
    localStorage.setItem('database', JSON.stringify(data))
    loadTodo()
  }

}

function getInfo(title){
    let database = JSON.parse(localStorage.getItem('database'))
    let detail = ""
    database.forEach(list => {
      if(list.title == title){
        detail = list
      }
    });

    console.log(detail)

    let date = detail.dueDate
    let newDate =""
    for(let i = 0; i < date.length; i++){
      if(date[i] !== "T"){
        newDate += date[i]
      }
      else{
        break;
      }
    }
    let value = 'Finish'
    if(detail.isFinish === true){
      value = 'Finish'
    }
    else{
      value = 'Not Yet'
    }
    $('#updateTodo').empty()
    $('#updateTodo').append(`
      <form class="seminor-login-form">
      <div class="form-group">
        <input type="name" id="detailTitle" class="form-control" value="${detail.title}" required autocomplete="off">
        <label class="form-control-placeholder" for="name">Title</label>
      </div>
      <div class="form-group">
        <input type="name" id="detailDescription" class="form-control" value="${detail.description}"  required autocomplete="off">
        <label class="form-control-placeholder" for="name">Description</label>
      </div>
      <div class="form-group">
        Due date : 
        <input type="date" " id="detailDate" width="276" value="${newDate}"/>
      </div>    
      <div class="form-group">
      <label for="exampleFormControlSelect1">isFinish : ${value}</label>
      <div class="actionButton">
        <div class="btn-check-log">
          <button type="submit" class="btn-add-todo" onclick="updateTodo('${detail.title}')"  data-toggle="tooltip" title="Update Todo" data-dismiss="modal">
            <span style="font-size:25px">
              <i class="fas fa-wrench"></i>
            </span>
          </button>
          <button type="submit" class="btn-add-todo" onclick="finishTodo('${detail.title}')" data-toggle="tooltip" title="Mark as Finish" data-dismiss="modal"> 
            <span style="font-size:25px">
              <i class="fas fa-calendar-check"></i>
            </span>
          </button>
          <button type="submit" class="btn-add-todo" onclick="unfinishTodo('${detail.title}')" data-toggle="tooltip" title="Mark as Unfinish" data-dismiss="modal">
            <span style="font-size:25px">
              <i class="fas fa-undo-alt"></i>
            </span>
          </button>
          <button type="submit" class="btn-add-todo" onclick="deleteTodo('${detail.title}')" data-toggle="tooltip" title="Delete Todo" data-dismiss="modal">
            <span style="font-size:25px">
              <i class="fas fa-trash-alt"></i>
            </span>
          </button>
        </div>
      </div>
  </form>
  `)
}

function updateTodo(title){
  let database = JSON.parse(localStorage.getItem('database'))
  let data = []
  let object =  {
    title : $('#detailTitle').val(),
    description : $('#detailDescription').val(),
    dueDate : $('#detailDate').val(),
    isFinish : ''
  }

  database.forEach(list => {
    if(list.title == title){
      object.isFinish = list.isFinish
      data.push(object)
    } else {
      data.push(list)
    }
  });

  localStorage.setItem('database', JSON.stringify(data))
  loadTodo()
}

function finishTodo(title){
  let database = JSON.parse(localStorage.getItem('database'))
  let data = []
  let object =  {
    title : $('#detailTitle').val(),
    description : $('#detailDescription').val(),
    dueDate : $('#detailDate').val(),
    isFinish : true
  }

  database.forEach(list => {
    if(list.title == title){
      data.push(object)
    } else {
      data.push(list)
    }
  });

  localStorage.setItem('database', JSON.stringify(data))
  loadTodo()
}

function unfinishTodo(title){
  let database = JSON.parse(localStorage.getItem('database'))
  let data = []
  let object =  {
    title : $('#detailTitle').val(),
    description : $('#detailDescription').val(),
    dueDate : $('#detailDate').val(),
    isFinish : false
  }

  database.forEach(list => {
    if(list.title == title){
      data.push(object)
    } else {
      data.push(list)
    }
  });

  localStorage.setItem('database', JSON.stringify(data))
  loadTodo()
}

function deleteTodo(title){
  let database = JSON.parse(localStorage.getItem('database'))
  let data = []

  database.forEach(list => {
    if(list.title != title){
      data.push(list)
    } 
  });

  localStorage.setItem('database', JSON.stringify(data))
  loadTodo()
}


